from flask import Flask, request, jsonify
from kaggle.api.kaggle_api_extended import KaggleApi
import pandas as pd
import os

app = Flask(__name__)

@app.route('/download', methods=['GET'])
def download_file():
    os.environ['KAGGLE_CONFIG_DIR'] = '/path/to/your/kaggle.json'
    dataset_slug = request.args.get('kaggle_path')

    api = KaggleApi()
    api.authenticate()
    api.dataset_download_files(dataset_slug, 'datasets', unzip=True)

    return jsonify([f'{dataset_slug} was downloaded successfully'])

@app.route('/files', methods=['GET'])
def get_files():
    files = []
    app.config['JSON_SORT_KEYS'] = False
    for file_name in os.listdir('datasets'):
        file_path = os.path.join('datasets', file_name)
        if os.path.isfile(file_path):
            df = pd.read_csv(file_path)
            columns = list(df.columns)
            files.append({
                'file_name': file_name,
                'columns_name': columns
            })

    return jsonify(files)

@app.route('/data', methods=['GET'])
def get_data():
    file_name = request.args.get('file')
    sort_by = request.args.get('sort_by')
    filter_by = request.args.getlist('filter_by')
    file_path = os.path.join('datasets', file_name)

    if os.path.isfile(file_path):
        df = pd.read_csv(file_path)
        if sort_by:
            df = df.sort_values(by=sort_by)
        if filter_by:
            columns = list(df.columns)
            selected_columns = [column for column in columns if column in filter_by[0]]
            return df[selected_columns].to_json(orient='records')

        return df.to_json(orient='records')
    else:
        return jsonify({'message': 'File not found.'}), 404

@app.route('/delete', methods=['GET'])
def delete_file():
    file_name = request.args.get('file')
    file_path = os.path.join('datasets', file_name)

    if file_name == 'all':
        file_list = os.listdir('datasets')
        for file in file_list:
            os.remove(os.path.join('datasets', file))
        return jsonify({'message': 'All files were deleted'})
    elif os.path.isfile(file_path):
        os.remove(file_path)
        return jsonify({'message': f'{file_name} was deleted'})
    else:
        return jsonify({'message': 'File not found.'}), 404

if __name__ == '__main__':
    app.run( host ='0.0.0.0', port=8080)
