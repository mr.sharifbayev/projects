#! /usr/bin/env python
import sys
import re


# reload(sys)
# sys.setdefaultencoding('utf-8') # required to convert to unicode

for line in sys.stdin:
    try:
        article_id, text = unicode(line.strip()).split('\t', 1)
    except ValueError as e:
        continue
    words = re.split("\W*\s+\W*", text, flags=re.UNICODE)
    for word in words:
        if 6 <= len(word) <= 15:
            word.replace(".","").replace("!","").replace("?","")
            word.replace(",","").replace("(","").replace(")","")

            if (6 <= len(word) <= 9
                and 65 <= ord(word[0]) <= 90
                    and word[1:] == word[1:].lower()):
                print >> sys.stderr, "reporter:counter:Wiki stats,Total words,%d" % 1
                print "%s\t%d" % (word.lower(), 1)
            else:
                print >> sys.stderr, "reporter:counter:Wiki stats,Total words,%d" % 1
                print "%s\t%d" % (word.lower(), -1)
