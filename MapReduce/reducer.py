#! /usr/bin/env python

import sys

current_key = None
word_sum = 0
word_sum_abs = 0

def Print():
    if current_key:
        if word_sum == word_sum_abs and word_sum_abs:
            print "%s\t%d" % (current_key, word_sum)
    pass

for line in sys.stdin:
    try:
        key, count = line.strip().split('\t', 1)
        count = int(count)
    except ValueError as e:
        continue
    if current_key != key:
        Print()
        word_sum = 0
        word_sum_abs = 0
        current_key = key
    word_sum += count
    word_sum_abs += abs(count)

Print()
