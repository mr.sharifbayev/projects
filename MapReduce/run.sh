#! /usr/bin/env bash

#OUT_DIR="my_wordcount_result_"$(date +"%s%6N")
OUT_DIR="hdfs0001_wordcount_result"
NUM_REDUCERS=7

hdfs dfs -rm -r -skipTrash ${OUT_DIR}* > /dev/null

yarn jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapred.job.name="Streaming wordCount" \
    -D mapreduce.job.reduces=${NUM_REDUCERS} \
    -files mapper.py,combiner.py,reducer.py \
    -mapper "python mapper.py" \
    -combiner "python combiner.py" \
    -reducer "python reducer.py" \
    -input /data/wiki/en_articles \
    -output ${OUT_DIR}_tmp > /dev/null

hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D stream.num.map.output.key.fields=2 \
    -D mapreduce.job.reduces=1 \
    -D mapreduce.job.output.key.comparator.class=org.apache.hadoop.mapreduce.lib.partition.KeyFieldBasedComparator \
    -D mapreduce.partition.keycomparator.options="-k2,2nr -k1,1" \
    -mapper cat \
    -reducer cat \
    -input ${OUT_DIR}_tmp \
    -output ${OUT_DIR} > /dev/null || echo "Errors"

# Checking result
#for num in `seq 0 $[$NUM_REDUCERS]`
#do
hadoop fs -cat ${OUT_DIR}/part-* | head
#done
