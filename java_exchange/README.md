# Instructions for running the program
You can run the entry point file located in src/main/java/com/company/currency/util/CurrencyRateApplication.java<br>
this is the entry point of the Spring Web application.
after running the program, you can perform one of the following tasks:<br>

## Example request conversion from base currency to target:
- if you want to convert amount of units from base currency to target currency, you can type the following link: <br>
  http://localhost:8080/api/exchange-rate/{base}/{target}?day={date}&amount={amount} <br>
  {amount} is replaced with the desired amount you want to converse<br>
- for example, if you entered the following endpoint: <br>
  http://localhost:8080/api/exchange-rate/USD/EUR?day=2009-05-21 <br>
  you will get the following output in json format :<br>
  {"rate":0.7262,"pair":"USD/EUR"} <br>
- for example, the following endpoint:<br>
  http://localhost:8080/api/exchange-rate/USD/RUB?day=2022-05-21&amount=10 <br> will give the following output: <br>
  {"rate":71.506,"pair":"USD/RUB","converted_amount":715.06"} <br>

## Example request statistics about number of requests:
- you can also send a request to find the number of times a currency was included as a target currency during runtime of the application.<br>
- this can be done by navigating to: http://localhost:8080/api/exchange-rate/statistics <br>
  which will give you something like this:<br>
  {"CHF":{"amount_of_requests":0},"HRK":{"amount_of_requests":0},"MXN":{"amount_of_requests":0},"ZAR":{"amount_of_requests":0},"INR":{"amount_of_requests":0},"CNY":{"amount_of_requests":0},"THB":{"amount_of_requests":0},"AUD":{"amount_of_requests":0},"ILS":{"amount_of_requests":0},"KRW":{"amount_of_requests":0},"JPY":{"amount_of_requests":0},"PLN":{"amount_of_requests":0},"GBP":{"amount_of_requests":0},"IDR":{"amount_of_requests":0},"HUF":{"amount_of_requests":0},"PHP":{"amount_of_requests":0},"TRY":{"amount_of_requests":0},"RUB":{"amount_of_requests":1},"ISK":{"amount_of_requests":0},"HKD":{"amount_of_requests":0},"EUR":{"amount_of_requests":3},"DKK":{"amount_of_requests":0},"USD":{"amount_of_requests":0},"CAD":{"amount_of_requests":0},"MYR":{"amount_of_requests":0},"BGN":{"amount_of_requests":0},"NOK":{"amount_of_requests":0},"RON":{"amount_of_requests":0},"SGD":{"amount_of_requests":0},"CZK":{"amount_of_requests":0},"SEK":{"amount_of_requests":0},"NZD":{"amount_of_requests":0},"BRL":{"amount_of_requests":0}}<br>

## Exception Handling:
- if you try to query on a currency that doesn't exist, the web page will return an error with the code 400, similar to the following: <br>
  {"status":400,"title":"ZFR is not in CURRENCY_LIST","description":""} <br>

- similarly, if the specified date doesn't contain information about exchange rate, the following will be displayed: <br>
  {"status":404,"title":"No Exchange Rate Available","description":""} <br>
