package com.company.currency.controller;

import com.company.currency.dto.CurrencyDTO;
import com.company.currency.dto.CurrencyRateDTO;
import com.company.currency.service.CurrencyRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class CurrencyRateController implements CurrencyRateApi {
  // TODO: implement.

  @Autowired
  CurrencyRateService currencyRateService;
  @GetMapping("exchange-rate/{target}/{base}")
  @Override public CurrencyRateDTO getRate(@PathVariable("base") String base,
                                           @PathVariable("target") String target,
                                           @RequestParam(name="amount", required = false) BigDecimal amount,
                                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                           @RequestParam(value = "day", required = false) Optional<LocalDate> day)
          throws RuntimeException {

    LocalDate date = day.orElse(LocalDate.now());
    return currencyRateService.getRate(base, target, amount, date);
  }

  @GetMapping("exchange-rate/statistics")
  @Override public Map<String, CurrencyDTO> getCurrencyStatistics() {

    return currencyRateService.getStatistics();

  }
}
