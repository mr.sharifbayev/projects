package com.company.currency.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CurrencyNotFoundException extends RuntimeException{
    public CurrencyNotFoundException(String currency) {
        super(String.format("%s %s", currency, " is not in CURRENCY_LIST"));
    }
}
