package com.company.currency.exceptions;

import com.company.currency.dto.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NoExchangeRateException extends RuntimeException{
    public NoExchangeRateException() {
        super();
    }
}
