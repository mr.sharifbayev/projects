package com.company.currency.service;

import com.company.currency.client.ExchangeRatesApi;
import com.company.currency.dto.CurrencyDTO;
import com.company.currency.exceptions.CurrencyNotFoundException;
import com.company.currency.dto.CurrencyRateDTO;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import com.company.currency.dto.ExchangeRatesResponse;
import com.company.currency.exceptions.NoExchangeRateException;
import com.company.currency.util.CurrencyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

// @Slf4j - for logging.
@Slf4j
@Service
public class CurrencyRateService implements ICurrencyRateService {
  // TODO: implement.
  final private HashMap<String, CurrencyDTO> statistic;

  CurrencyRateService() {
    HashMap<String, CurrencyDTO> statistic = new HashMap<>();
    for (String currency: CURRENCY_LIST) {
      statistic.put(currency, new CurrencyDTO());
    }
    this.statistic = statistic;
  }

  @Autowired
  ExchangeRatesApi exchangeRatesApi;
  private void checkingExistence(String currency) throws CurrencyNotFoundException {

    if (!ICurrencyRateService.CURRENCY_LIST.contains(currency)) {
      throw new CurrencyNotFoundException(currency);
    }
  }

  private BigDecimal getExchangeRateForEUR(LocalDate date, String currency) throws NoExchangeRateException {
    if (currency.equals("EUR")) {
      return BigDecimal.ONE;
    }
    ResponseEntity<ExchangeRatesResponse> responseEntity =
            exchangeRatesApi.getCurrencyRate(date, date, currency);
    try {
      return responseEntity.getBody().getDataSets().get(0).getSeries()
              .getFirstSeries().getObservations().getRate().get(0);
    } catch (Exception ex) {
      throw new NoExchangeRateException();
    }
  }

  private BigDecimal getExchangeRate(String base, String target, LocalDate date)
          throws CurrencyNotFoundException, NoExchangeRateException {
    checkingExistence(base);
    statistic.get(base).incAmountOfRequests();
    checkingExistence(target);
    statistic.get(target).incAmountOfRequests();
    if (!base.equals(target)) {
      BigDecimal baseRate = getExchangeRateForEUR(date, base);
      BigDecimal targetRate = getExchangeRateForEUR(date, target);
      return CurrencyUtils.calcRate(baseRate, targetRate);
    }
    return BigDecimal.ONE;
  }
  @Override public CurrencyRateDTO getRate(String base, String target, BigDecimal amount, LocalDate date)
          throws CurrencyNotFoundException, NoExchangeRateException {

    CurrencyRateDTO currencyRate = new CurrencyRateDTO();
    currencyRate.setPair(String.format("%s/%s", base, target));
    BigDecimal exchangeRate = getExchangeRate(base, target, date);
    currencyRate.setRate(exchangeRate);
    if (amount != null) {
      currencyRate.setConvertedAmount(exchangeRate.multiply(amount));
    }

    return currencyRate;
  }

  @Override public Map<String, CurrencyDTO> getStatistics() {

    return statistic;
  }
}

