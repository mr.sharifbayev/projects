import parsing_numbers
import numpy as np
import os, shutil


def jpg_to_txt():
    image_folder = 'imgs'
    img, _ = parsing_numbers.load_imgs(image_folder)
    etalon_nums, _ = parsing_numbers.load_imgs('numbers')
    numbers = parsing_numbers.drop_to_numbers(img, [23234])

    parse_captcha = []
    for num in numbers:
        err = []
        for e_num in etalon_nums:
            error = abs(num - e_num)
            err.append(error.sum())
        parse_captcha.append(str(np.array(err).argmin()))

    answ = ''.join(parse_captcha)
    # print(answ)

    for filename in os.listdir(image_folder):
        file_path = os.path.join(image_folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

    return answ

# jpg_to_txt()