#!/usr/bin/env python3
import asyncio
from telethon import TelegramClient
import jpgToTxt
import time
from datetime import datetime
import re
import os

clients = [
    {"session_name": 'xxx', "api_id": 123, "api_hash": 'xxx',
     "phone_number": 'xxx'},
]

BOT = '@nameBot'


async def send_message(name, phone_number, api_id, api_hash):
    async with TelegramClient(name, api_id, api_hash) as client:
        if not await client.is_user_authorized():
            await client.start(phone_number)

        await client.send_message(BOT, 'Claim')
        await asyncio.sleep(1)

        async for message in client.iter_messages(BOT, limit=2):
            if message.media:
                await client.download_media(message, "imgs/")

        await asyncio.sleep(2)
        message_text = jpgToTxt.jpg_to_txt()
        await client.send_message(BOT, message_text)


def delta_time():
    time_pattern = re.compile(r'(\d{2}:\d{2}:\d{2})')
    last_time = None
    with open('time.log', 'r') as f:
        for line in f:
            match = time_pattern.search(line)
            if match:
                last_time = datetime.strptime(match.group(1), '%H:%M:%S')

    if last_time:
        current_time = datetime.strptime(datetime.now().strftime('%H:%M:%S'), '%H:%M:%S')
        time_difference = (current_time - last_time).total_seconds()
        delta = time_difference - 1800
        if delta < 0:
            return abs(delta) + 1
    return 1


async def main():
    print(datetime.strptime(datetime.now().strftime('%H:%M:%S'), '%H:%M:%S'))
    print(delta_time())
    time.sleep(delta_time())
    for client_data in clients:
        await send_message(client_data['session_name'], client_data['phone_number'],
                           client_data['api_id'], client_data['api_hash'])

    current_time = datetime.now().strftime('%H:%M:%S')
    with open('time.log', 'w') as f:
        f.write(f"{current_time}\n")


asyncio.run(main())
