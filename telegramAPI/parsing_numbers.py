import numpy as np
import os
import cv2

def crop_image(img):
    x, y, w, h = 17, 11, 45, 12
    cropped_img = img[y:y + h, x:x + w]
    return cropped_img

def drop_to_numbers(imgs, labels):
    img_numbers = []
    numbers = {}

    for j, img in enumerate(imgs):
        crop_img = crop_image(img)
        x, y, w, h = 0, 0, 9, 12
        label = str(labels[j])
        for i in range(5):
            num_img = crop_img[y:y + h, x:x + w]
            num_img[num_img > 209] = 255
            num_img[num_img <= 209] = 0
            img_numbers.append(num_img)
            x += w
            # numbers[int(label[i])] = num_img

    return img_numbers

def load_imgs(image_folder):
    image_files = [f for f in os.listdir(image_folder) if os.path.isfile(os.path.join(image_folder, f))]
    image_files.sort()
    images = []
    labels = []

    for image_file in image_files:
        image_path = os.path.join(image_folder, image_file)
        image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
        images.append(image)
        # label = int(os.path.splitext(image_file)[0])
        # labels.append(label)

    images = np.array(images)
    # labels = np.array(labels)

    return images, labels

def main():
    image_folder = 'dataset'
    images, labels = load_imgs(image_folder)
    nums = drop_to_numbers(images, labels)
    keys = [0,1,2,3,4,5,6,7,8,9]
    for key in keys:
        cv2.imwrite(f"numbers/{key}.png", nums[key])
        print(key, nums[key])

# main()


